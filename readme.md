# Kiri Search Library

###

## Getting Started

Install via npm:

`npm install kiri-search-library`

Install via CDN:

```html
<script src="https://unpkg.com/kiri-search-library"></script>
```

## How to use

1. Initialize the Kiri agent with a provided API key.

```javascript
var agent = new KiriAgent("pk_yourAPIKey");
```

2. Make a search request

```javascript
var results = await agent.search("Some query", num_results);
```

3. Redirect the user to the results

```javascript
// Redirect the user to the first search result.
agent.redirect(results[0].url, results[0].id);
```

## API - KiriAgent class

### `constructor(apiKey)`

Creates a KiriAgent object

- **apiKey (string)**: The provided public facing API key for a specified agent.

### `search(q, n, f, o)`

Searches through the agent's articles with the query, q, and returns the most relevant n articles.

- **q (string)**: The query to use to search through the articles
- **n (number)**: The number of results to return (should be a number between 1 and 10 inclusive).
- **f (object: optional)**: The filters to be used when returning the search results.
- **p (number: optional)**: A zero-indexed value that refers to the page number of relevant results to return (e.g. page 10 has an index of 9).

### `redirect(url, article_id)`

Redirects the browser to the specified article and logs the redirection as an analytic which can be viewed in the agent's dashboard.

- **url (string)**: The url of the redirected article.
- **article_id (string)**: The ID of the article being redirected to (necessary for logging).
