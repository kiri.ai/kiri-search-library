declare module "kiri-search-library" {
  export interface SearchResponse {
    totalResults: number;
    totalPages: number;
    page: number;
    pageSize: number;
    results: QueryResult[];
  }

  export interface QueryResult {
    id: string;
    title: string;
    answer: string;
    metatext: string;
    url: string;
    score: number;
    attributes: {
      Language?: string;
      Category?: string;
    };
  }

  export interface Filters {
    Language: string;
  }

  export default class KiriAgent {
    constructor(api_key: string);
    search(
      q: string,
      n: number,
      f?: Filters,
      p?: number,
    ): Promise<SearchResponse> | undefined;
    redirect(url: string, article_id: string): undefined | void;
  }
}
