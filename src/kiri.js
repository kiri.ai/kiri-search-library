// The Kiri library
"use strict";

(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define([], factory) :
	(global = global || self, global.KiriAgent = factory());
}(this, (function () {
  const cookieKey = "kiriUserID";
  const searchUrl = "https://api.kiri.ai";

  let apiKey;
  let userID;

  const setCookie = (name, value, options) => {
    document.cookie = `${name}:${value}; expires=${options.expires}`;
  };

  const getCookie = (name) => {
    let cookies = "; " + document.cookie;
    if (!cookies.includes(`; ${name}:`)) {
      return undefined;
    }

    // Get the 2nd value
    let remaining = cookies.split(`; ${name}:`)[1];
    // Get the first value before the split to get the selected cookie.
    let cookie = remaining.split(";")[0];
    return cookie;
  };

  const isUUID = (uuid) => {
    let s = "" + uuid;

    let match = s.match(
      "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
    );
    if (match === null) {
      return false;
    }
    return true;
  };

  const createOrUpdateUser = async () => {
    const { ips, countries, user_agents, locations } = await getUserInfo();

    const userRequest = {
      method: "PUT",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: new URLSearchParams({
        id: userID,
        countries: countries,
        ips: ips,
        user_agents: user_agents,
        locations: locations,
      }),
    };
    try {
      const fetchResponse = await fetch(
        `${searchUrl}/users?kiri-api-key=key`,
        userRequest
      );
      const data = await fetchResponse.json();
      if (data.id) {
        let id = data["id"];
        setCookie(cookieKey, id, { expires: new Date(2111, 11, 11) });
        userID = id;
      }
    } catch (e) {}
  };

  const getUserInfo = async () => {
    var ips = [];
    var countries = [];
    var user_agents = [];
    var locations = [];
    const cfRequest = {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    };

    try {
      const cfResponse = await fetch(
        "https://www.cloudflare.com/cdn-cgi/trace",
        cfRequest
      );
      const d = await cfResponse.text();
      var data = d.replace(/[\r\n]+/g, '","').replace(/\=+/g, '":"');
      data = '{"' + data.slice(0, data.lastIndexOf('","')) + '"}';
      var jsondata = JSON.parse(data);
      ips = [jsondata["ip"]];
      countries = [jsondata["loc"]];
      user_agents = [jsondata["uag"]];
      locations = [jsondata["colo"]];

      return { ips, countries, user_agents, locations };
    } catch (e) {
      return { ips, countries, user_agents, locations };
    }
  };

  const agent = class {
    constructor(api_key) {
      if (!api_key || typeof api_key != "string") {
        console.error("Api key is not defined.");
        return;
      }
      apiKey = api_key;
      userID = getCookie(cookieKey);
      if (!isUUID(userID)) {
        userID = null;
      }

      createOrUpdateUser();
    }

    /**
     * Makes a search through the agent's documents with the specified query.
     * Returns the most relevant n results.
     *
     * @param {string} q
     * @param {number} n
     * @param {object} f
     * @param {object} p
     */
    async search(q, n, f, p) {
      if (!apiKey || typeof apiKey != "string") {
        console.error("Api key is not defined.");
        return;
      }
      if (q == null || typeof q != "string") {
        console.error("Invalid value for query.");
        return;
      }
      if (n == null || n > 10 || n < 1 || typeof n != "number") {
        console.error(
          "Invalid value for n specified. Must be a value between 1 and 10 inclusive."
        );
        return;
      }
      if (p != null && ( p < 0 || typeof p != "number" )){
        console.error("Invalid value for p specified. Must be a number greater or equal to 0.");
        return;
      }
      

      const params = new URLSearchParams({
        query: q,
        results: n,
        userID: userID,
        filters: f == null ? "" : JSON.stringify(f),
        page: p == null ? 0: p,
      });
      const searchReq = {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: params,
      };

      const response = await fetch(
        `${searchUrl}/search?kiri-api-key=${apiKey}`,
        searchReq
      );

      if (!response.ok) {
        console.error(await response.text());
        return;
      }

      return await response.json();
    }

    /**
     * Redirects the webpage to one of the specified results.
     * Logs the redirections as analytics on the kiri dashboard.
     *
     * @param {string} url
     * @param {string} article_id
     */
    redirect(url, article_id) {
      if (!apiKey || typeof apiKey != "string") {
        console.error("Api key is not defined.");
        return;
      }
      if (!url || typeof url != "string") {
        console.error("Invalid value for url.");
        return;
      }
      if (!article_id || typeof article_id != "string") {
        console.error("Invalid value for article id.");
        return;
      }

      const params = new URLSearchParams({
        article_id: article_id,
        user_id: userID,
      });

      // Log click to the dashboard.
      const logReq = {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: params,
      };

      // Log the redirection event asynchronously.
      fetch(`${searchUrl}/clicks?kiri-api-key=${apiKey}`, logReq)
        .then((res) => {
          return res;
        })
        .catch((error) => {
          console.error(error);
        });

      window.location = url;
    }
  };
    return agent
})));
